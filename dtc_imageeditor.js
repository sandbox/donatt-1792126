(function($) {
  // Namespace for the module.
  Drupal.dtcImageeditor = {};
  // This object contains the jQuery objects of the used HTML elements.
  Drupal.dtcImageeditor.elements = {};
  // Container for "global" events.
  Drupal.dtcImageeditor.events = {};

  // Shorthand for the elements.
  var e = Drupal.dtcImageeditor.elements;

  /**
   * Register the module's behaviors to Drupal.behaviors.
   */
  Drupal.behaviors.dtcImageeditor = {
    attach: function (context, settings) {
      e.bgImageField = $('#background-image-field');
      e.editedCheckbox = $('#edit-background-image-edited');
      e.editedCheckboxLabel = $('label[for="edit-background-image-edited"]');

      // When the DTC module is ready, modify the background image field.
      $(Drupal.dtcImageeditor.events).bind('Drupal.dtcImageeditor.dtcReady', 
        function() {
          Drupal.dtcImageeditor.convertImageField();
      });

      // If the image URL is modified in the bgImage field, check whether it is
      // edited or not and modify the field widget accordingly.
      $(Drupal.dtcImageeditor.events).bind('Drupal.dtcImageeditor.contextModified', 
        function() {
          Drupal.dtcImageeditor.checkImageIsEdited();
      });
      
    }
  };

  /**
   * Converts Dtc editor's background image field to an editable image field.
   */
  Drupal.dtcImageeditor.convertImageField = function() {
    // wrap the imagefield
    e.bgImageField.wrap('<div id="editable-background-image-field">'
                        + '<div class="col"></div></div>');

    e.bgImageFieldWidget = $('#editable-background-image-field');
    e.bgImageFieldWidget.css('width', e.bgImageField.css('width'));
    e.bgImageField.css('width', '100%');

    // create an other column for the edit/revert button
    e.bgImageFieldWidget.append('<div class="col" />');
    var widgetsRightCol = e.bgImageFieldWidget.children('.col:last-child');

    // move the checkbox and its label to their position
    e.editedCheckbox.remove().appendTo(widgetsRightCol);
    e.editedCheckboxLabel.remove().insertAfter(e.editedCheckbox);

    // convert edited checkbox to jQuery UI button
    e.editedCheckbox.button();
    e.editButton = e.editedCheckbox.button('widget');

    Drupal.dtcImageeditor.setEventHandlers();
    Drupal.dtcImageeditor.checkImageIsEdited();
  };

  /**
   * Sets event handlers for events which are not set as behaviours.
   */
  Drupal.dtcImageeditor.setEventHandlers = function() {
    e.editedCheckbox.change(Drupal.dtcImageeditor.toggleEdited);
  };

  /**
   * Sends an AJAX request to the server based on the parameters.
   * 
   * @param url string
   * The URL which should handle the request.
   * @param successHandler function
   * This function will be called if the request succeeded.
   * @param additionalData object
   * Request contains base data, with this parameter these sent parameters
   * can be extended.
   * 
   * @return bool
   * Shows whether the reques was sent or not (e.g. due to a missing parameter).
   */
  Drupal.dtcImageeditor.doRequest = function(url, successHandler,
                                             additionalData
  ) {
    var imageUrl = e.bgImageField.val();
    imageUrl = Drupal.dtcImageeditor.unflagImageUrl(imageUrl);

    if (typeof(imageUrl) === 'undefined' || imageUrl === '') {
      return false;
    }
    
    var data = {
        image_url: imageUrl,
        theme: Drupal.dtcImageeditor.getTheme(),
        selector: Drupal.dtcImageeditor.getSelector()
    };
    
    // If there is additional data for the request, send it as well.
    if (typeof(additionalData) === 'object') {
      $.extend(data, additionalData);
    }
    
    $.ajax({
      type: 'POST',
      url: url,
      data: data,
      success: successHandler,
      error: function() {
        alert('dtc_imageeditor (code 1): '
            + Drupal.t('Something went wrong.'));
      },
      dataType: 'json'
    });
    
    return true;
  };
  
  /**
   * Sends a request to check whether the image in the
   * background image field is edited or not.
   */
  Drupal.dtcImageeditor.checkImageIsEdited = function() {
    var imageUrl = e.bgImageField.val();
    if (!Drupal.dtcImageeditor.isEditedImageUrl(imageUrl)) {
      Drupal.dtcImageeditor.setEdited(false);
      return;
    }
    
    var sent = Drupal.dtcImageeditor.doRequest(
      Drupal.settings.dtcImageeditor.ajaxIsEditedUrl,
      Drupal.dtcImageeditor.checkImageIsEditedSuccess);
    
    // If the request was not sent it means that the parameters were not valid,
    // and so the image can't be an edited one.
    if (!sent) {
      Drupal.dtcImageeditor.setEdited(false);
    }
  };

  /**
   * Handler for checkImageIsEdited() request.
   * 
   * If there was no errror the function updates the background image field's
   * state (edited / not edited) based on the response.
   * 
   * @param data object
   * The data returned by the server.
   */
  Drupal.dtcImageeditor.checkImageIsEditedSuccess = function(data) {
    if (data.error || typeof(data.value) === 'undefined') {
      alert('dtc_imageeditor (code 2): '
            + Drupal.t('Something went wrong.'));
      Drupal.dtcImageeditor.setEdited(false);
      return;
    }

    // note: data.value is not boolean
    var edited = data.value ? true : false;
    Drupal.dtcImageeditor.setEdited(edited);
  };
  
  /**
   * Sends a request to make the image edited (editable).
   */
  Drupal.dtcImageeditor.editImage = function() {
    var sent = Drupal.dtcImageeditor.doRequest(
      Drupal.settings.dtcImageeditor.ajaxEditUrl,
      Drupal.dtcImageeditor.editImageSuccess);

    if (!sent) {
      Drupal.dtcImageeditor.setEdited(false);
    }
  };
  
  /**
   * Handler for editImage() request.
   * 
   * If the request succeeded, the function converts the background image
   * field to an edited field (with image editor links).
   * 
   * @param data object
   * The data returned by the server.
   */
  Drupal.dtcImageeditor.editImageSuccess = function(data) {
    if (data.error || typeof(data.editableImageUrl) === 'undefined') {
      alert('dtc_imageeditor (code 3): '
            + Drupal.t('Something went wrong.'));
      Drupal.dtcImageeditor.setEdited(false);
      return;
    }

    Drupal.dtcImageeditor.updateImageField(data.editableImageUrl, true);
  };
  
  /**
   * Sends a request to revert the edited image.
   */
  Drupal.dtcImageeditor.revertImage = function() {
    var sent = Drupal.dtcImageeditor.doRequest(
      Drupal.settings.dtcImageeditor.ajaxRevertUrl,
      Drupal.dtcImageeditor.revertImageSuccess);

    if (!sent) {
      Drupal.dtcImageeditor.setEdited(true);
    }
  };
  
  /**
   * Handler for revertImage() request.
   * 
   * If the request succeeded, the function converts the backround image field
   * back to a plain input field.
   * 
   * @param data object
   * The data returned by the server.
   */
  Drupal.dtcImageeditor.revertImageSuccess = function(data) {
    if (data.error || typeof(data.originalImageUrl) === 'undefined') {
      alert('dtc_imageeditor (code 4): '
            + Drupal.t('Something went wrong.'));
      Drupal.dtcImageeditor.setEdited(true);
      return;
    }

    Drupal.dtcImageeditor.updateImageField(data.originalImageUrl, false);
  };
  
  /**
   * The callback which handles saving of the images
   * edited by an editor service.
   */
  Drupal.dtcImageeditor.saveEditedImage = function() {
    Drupal.dtcImageeditor.doRequest(
      Drupal.settings.dtcImageeditor.ajaxSaveUrl,
      Drupal.dtcImageeditor.saveEditedImageSuccess,
      {image_data: Drupal.settings.imageeditor.save.image}
    );
  };
  
  /**
   * Handler for saveEditedImage() request.
   * 
   * The function displays an error message if the
   * edited image could not be saved.
   * 
   * @param data object
   * The data returned by the server.
   */
  Drupal.dtcImageeditor.saveEditedImageSuccess = function(data) {
    if (data.error) {
      alert('dtc_imageeditor (code 5): '
            + Drupal.t('Something went wrong.'));
      return;
    }

    // Updates the image's URL to bypass cached image.
    var imageUrl = e.bgImageField.val();
    Drupal.dtcImageeditor.updateImageField(imageUrl, true);
  };

  /**
   * Changes the background image field's
   * state based on the "edited" parameter.
   * 
   * @param edited bool
   * If it is true, the background image field will be edited (editors 
   * will be shown), if it is false, the it will be a plain input field.
   */
  Drupal.dtcImageeditor.setEdited = function(edited) {
    if (e.bgImageFieldWidget.data('edited') != edited) {
      if (edited) {
        e.bgImageField.hide();
        e.editedCheckbox.button('option', 'label', Drupal.t('Revert'));
      }
      else {
        e.bgImageField.show();
        e.editedCheckbox.button('option', 'label', Drupal.t('Edit'));
      }

      // save the state of the widget
      e.bgImageFieldWidget.data('edited', edited);
    }
    Drupal.dtcImageeditor.displayImageEditor(edited);
    Drupal.dtcImageeditor.updateEditButton(edited);
  };

  /**
   * Updates the "edited" checkbox and jQuery UI button.
   * 
   * The jQuery UI button's state follows the user's actions (as the the
   * underlying checkbox element), not the AJAX calls, so if an error happens,
   * we have to manually synchronize its state with the image field's
   * actual state.
   * 
   * @param edited bool
   * Shows whether we would like to set the checkbox and jQuery UI button to
   * checked or unchecked.
   */
  Drupal.dtcImageeditor.updateEditButton = function(edited) {
    if (e.editedCheckbox.is(':checked') != edited) {
      e.editedCheckbox.attr('checked', edited);
      e.editedCheckbox.button('refresh');
    }
  }

  /**
   * Displays / hides the image editor buttons.
   * 
   * If the editor is not added already, this function creates it,
   * if it is present, the function updates the edited image's URL to reflect
   * the currently selected element's background image.
   * 
   * @param display bool
   */
  Drupal.dtcImageeditor.displayImageEditor = function(display) {
    if (display) {
      var imageUrl = e.bgImageField.val();
      imageUrl = Drupal.dtcImageeditor.unflagImageUrl(imageUrl);
      if (e.imageEditorDiv == null) {
        Drupal.dtcImageeditor.addImageEditor(imageUrl);
      }
      else {
        e.imageEditorDiv.show();
        Drupal.dtcImageeditor.changeImageInEditor(imageUrl);
      }
    }
    else {
     if (e.imageEditorDiv != null) {
        e.imageEditorDiv.hide();
     }
    }
  };

  /**
   * Creates the image editor menu and adds it to the editor div.
   * 
   * @param imageUrl string
   * The URL of the image that should be edited by the editors.
   */
  Drupal.dtcImageeditor.addImageEditor = function(imageUrl) {
    var options = {
      editors: Drupal.settings.dtcImageeditor.editors,
      uploaders: Drupal.settings.dtcImageeditor.uploaders,
      image: {url: imageUrl},
      $element: e.bgImageField,
      callback: Drupal.dtcImageeditor.saveEditedImage
    };
    Drupal.imageeditor.initialize(options);

    e.imageEditorDiv = e.bgImageFieldWidget.find('div.imageeditor');
  };

  /**
   * Changes the edited image's URL in the
   * editor menu'shidden parameter field.
   * 
   * @param imageUrl string
   * The URL of the image that should be edited by the editors.
   */
  Drupal.dtcImageeditor.changeImageInEditor = function(imageUrl) {
    e.imageEditorDiv.find('input[type="hidden"].url')
                    .val(imageUrl);
  }

  /**
   * Toggles whether the image is edited or not.
   * 
   * This function is used as an event handler.
   */
  Drupal.dtcImageeditor.toggleEdited = function() {
    if (!e.bgImageFieldWidget.data('edited')) {
      Drupal.dtcImageeditor.editImage();
    }
    else {
      Drupal.dtcImageeditor.revertImage();
    }
  };

  /**
   * Updated the image field based on the given parameters.
   * 
   * If the image is "edited" it will be flagged (with a timestamp)
   * to bypass browser cache and display the latest version.
   * 
   * @param imageUrl string
   * The URL of the image, this will be written into the
   * background image input field.
   * @param edited boolean
   * Shows whether the image is edited or not.
   */
  Drupal.dtcImageeditor.updateImageField = function(imageUrl, edited) {
    if (edited) {
      // If the image is an edited one, bypass the cache
      // by adding a timestamp to its URL.
      imageUrl = Drupal.dtcImageeditor.flagImageUrl(imageUrl);
    }
    e.bgImageField.val(imageUrl);
    // act as if the user changed the image so that the Dtc module update the css
    e.bgImageField.trigger('change');
    Drupal.dtcImageeditor.setEdited(edited);
  }

  /**
   * Checks whether the given image URL can be an
   * edited image (based on pattern matching).
   * 
   * @param imageUrl string
   * The image URL to check.
   * 
   * @return boolean
   * true if the image can be an edited image based on its URL,
   * false otherwise.
   */
  Drupal.dtcImageeditor.isEditedImageUrl = function(imageUrl) {
    if (typeof(imageUrl) !== 'undefined' &&
        imageUrl.indexOf(Drupal.settings.dtcImageeditor.editedImagePattern) >= 0
    ) {
        return true;
    }
    
    return false;
  };
  
  /**
   * Adds a timestamp flag to the image URL if it is (can be) an edited image,
   * otherwise the function simply returns the URL unchanged.
   * 
   * It is necessary to bypass the cached image and thus
   * show the latest version.
   *
   * @param imageUrl string
   * The imageUrl to flag.
   * 
   * @return string
   * The flagged URL if the image is an edited image or
   * the unchanged URL if it isn't.
   */
  Drupal.dtcImageeditor.flagImageUrl = function(imageUrl) {
    if (!Drupal.dtcImageeditor.isEditedImageUrl(imageUrl)) {
      return imageUrl;
    }
    else {
      // First remove the previously set flag if any.
      imageUrl = Drupal.dtcImageeditor.unflagImageUrl(imageUrl);
      return imageUrl + '?timeFlag=' + (new Date().getTime());
    }
  };

  /**
   * Removes the timestamp flag from an image URL if it is (can be)
   * an edited image, otherwise the function simply returns the URL unchanged.
   * 
   * @param imageUrl string
   * The imageUrl to un-flag.
   * 
   * @return string
   * The un-flagged URL if the image is an edited image or
   * the unchanged URL if it isn't.
   */
  Drupal.dtcImageeditor.unflagImageUrl = function(imageUrl) {
    if (!Drupal.dtcImageeditor.isEditedImageUrl(imageUrl)) {
      return imageUrl;
    }
    else {
      var flagBeginning = imageUrl.lastIndexOf('?timeFlag=');
      if (flagBeginning < 0) {
        flagBeginning = imageUrl.length;
      }
      return imageUrl.substring(0, flagBeginning);
    }
  };

  /**
   * Gets the current theme from Dtc module.
   * 
   * @return string
   * The current theme.
   */
  Drupal.dtcImageeditor.getTheme = function() {
    return Drupal.settings.dtc.theme;
  };
  
  /**
   * Gets the currently edited element's selector from the Dtc module.
   * 
   * @return string
   * The currently edited element's selector.
   */
  Drupal.dtcImageeditor.getSelector = function() {
    return Dtc.editor.currentlyEditing[Dtc.editor.currentlySelected];
  };

  // Hack: create dispatch events when certain Dtc functions are called.
  if (typeof(Dtc) !== 'undefined') {
    var dtcEnableFunction = Dtc.enable;
    if ($.isFunction(dtcEnableFunction)) {
      Dtc.enable = function() {
        dtcEnableFunction.apply(this, arguments);
        $(Drupal.dtcImageeditor.events).trigger('Drupal.dtcImageeditor.dtcReady');
      }
    }

    var context = null;
    var dtcLoadCssPropsFunction= Dtc.loadCssProps;
    if ($.isFunction(dtcLoadCssPropsFunction)) {
      Dtc.loadCssProps = function() {
        dtcLoadCssPropsFunction.apply(this, arguments);

        var newContext = Drupal.dtcImageeditor.getTheme()
                      + '_'
                      + Drupal.dtcImageeditor.getSelector();
        if (context != newContext) {
          context = newContext;
          $(Drupal.dtcImageeditor.events).trigger('Drupal.dtcImageeditor.contextModified');
        }
      }
    }
  }
})(jQuery);
