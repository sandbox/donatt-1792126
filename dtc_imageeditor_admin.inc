<?php

/**
 * Implements hook_form().
 * 
 * Copied from Image Editor Inline module.
 * 
 * @return array
 * The settings form. 
 */
function dtc_imageeditor_settings_form() {
  $form = array();
  $form['editors'] = array(
    '#type' => 'item',
    '#title' => t('Enabled Image Editors'),
    '#description' => t('Choose enabled image editors for inline image editing and their order.'),
  );

  $position = 0;
  foreach (imageeditor_info() as $codename => $editor) {
    $position++;
    $form['dtc_imageeditor_' . $codename . '_enabled'] = array(
      '#type' => 'checkbox',
      '#title' =>  '<div class="imageeditor-item imageeditor-editor ' . $editor['class'] . '"></div>' . $editor['name'] . ' (<a href="' . $editor['site'] . '" target="_blank">' . t('site') . '</a>)',
      '#default_value' => variable_get('dtc_imageeditor_' . $codename . '_enabled', 0),
    );
    $form['dtc_imageeditor_' . $codename . '_description'] = array(
      '#type' => 'markup',
      '#markup' => $editor['description'],
    );
    $form['dtc_imageeditor_' . $codename . '_api_key'] = array(
      '#type' => 'markup',
      '#markup' => $editor['api_key'] ? l(variable_get($editor['api_key_codename']) ? t('Already set'): t('Required'), 'admin/config/media/imageeditor') : t('Not needed'),
    );
    $form['dtc_imageeditor_' . $codename . '_position'] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('dtc_imageeditor_' . $codename . '_position', 0) ? variable_get('dtc_imageeditor_' . $codename . '_position', 0) : $position,
      '#size' => 3,
      '#maxlenth' => 4,
      '#attributes' => array('class' => array('imageeditor-position')),
    );
  }

  $form['uploaders'] = array(
    '#type' => 'item',
    '#title' => t('Enabled Upload services'),
    '#description' => t('Choose enabled upload services to upload images to if your images are not available from external network.'),
  );

  $position = 0;
  foreach (imageeditor_uploaders() as $codename => $uploader) {
    $position++;
    $form['dtc_imageeditor_' . $codename . '_enabled'] = array(
      '#type' => 'checkbox',
      '#title' =>  '<div class="imageeditor-item imageeditor-uploader ' . $uploader['class'] . '"></div>' . $uploader['name'] . ' (<a href="' . $uploader['site'] . '" target="_blank">' . t('site') . '</a>)',
      '#default_value' => variable_get('dtc_imageeditor_' . $codename . '_enabled', 0),
    );
    $form['dtc_imageeditor_' . $codename . '_description'] = array(
      '#type' => 'markup',
      '#markup' => $uploader['description'],
    );
    $form['dtc_imageeditor_' . $codename . '_api_key'] = array(
      '#type' => 'markup',
      '#markup' => $uploader['api_key'] ? l(variable_get($uploader['api_key_codename']) ? t('Already set'): t('Required'), 'admin/config/media/imageeditor') : t('Not needed'),
    );
    $form['dtc_imageeditor_' . $codename . '_position'] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('dtc_imageeditor_' . $codename . '_position', 0) ? variable_get('dtc_imageeditor_' . $codename . '_position', 0) : $position,
      '#size' => 3,
      '#maxlenth' => 4,
      '#attributes' => array('class' => array('imageuploader-position')),
    );
  }

  $form['#theme'] = 'dtc_imageeditor_settings_form';
  return system_settings_form($form);
}

/**
 * Themes dtc_imageeditor_settings_form.
 * 
 * Copied from Image Editor Inline module.
 * 
 * @param array $variables
 * An array which contains the form's elements to be themed.
 * 
 * @return string
 * The rendered form.
 */
function theme_dtc_imageeditor_settings_form($variables) {
  $form = $variables['form'];
  $output = drupal_render($form['editors']);

  $header = array(t('Name'), t('Description'), t('API key'), t('Position'));
  $rows = array();
  $imageeditors = imageeditor_info();
  $imageeditors_sort = array();
  foreach ($imageeditors as $codename => $editor) {
    $imageeditors_sort[$codename] = $form['dtc_imageeditor_' . $codename . '_position'];
  }
  asort($imageeditors_sort);

  foreach ($imageeditors_sort as $codename => $position) {
    $row = array();
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_enabled']);
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_description']);
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_api_key']);
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_position']);
    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }
  drupal_add_tabledrag('imageeditors-table', 'order', 'sibling', 'imageeditor-position');
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'imageeditors-table')));

  $output .= drupal_render($form['uploaders']);
  $rows = array();
  $imageuploaders = imageeditor_uploaders();
  $imageuploaders_sort = array();
  foreach ($imageuploaders as $codename => $uploader) {
    $imageuploaders_sort[$codename] = $form['dtc_imageeditor_' . $codename . '_position'];
  }
  asort($imageuploaders_sort);

  foreach ($imageuploaders_sort as $codename => $position) {
    $row = array();
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_enabled']);
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_description']);
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_api_key']);
    $row[] = drupal_render($form['dtc_imageeditor_' . $codename . '_position']);
    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }
  drupal_add_tabledrag('imageuploaders-table', 'order', 'sibling', 'imageuploader-position');
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'imageuploaders-table')));

  $output .= drupal_render_children($form);
  return $output;
}