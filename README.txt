README.txt
======================

This module integrates Image Editor into Drupal Theme Creator.

Dependencies:

You must have:
  - Dtc (http://drupal.org/project/dtc) and
  - Image Editor (http://drupal.org/project/imageeditor) 
modules installed and enabled.

Installation and setup of Dtc Image Editor:

1. Install and enable the module as usual.
2. Allow accessing the module's features to user groups on
   [your site]/admin/people/permissions page:
     - Use Dtc Image Editor: Users whith this permission can edit background
       images defined with the Dtc module's toolbar, but can not modify
       Dtc Image Editor's preferences on administrative pages.
     - Administer Dtc Image Editor: Users whith this permission can change
       preferences of this module on administrative pages, e.g. they can choose
       the available image editor services.
3. Choose the editor services you wish to use on Image Editor's config page:
   [your site]/admin/config/media/imageeditor/dtc_imageeditor

After that when you enable Dtc's toolbar on
[your site]/admin/config/development/dtc page
you will be able to edit background images. (Note the "Edit" button next to the
Image URL field on the toolbar's Background tab).
